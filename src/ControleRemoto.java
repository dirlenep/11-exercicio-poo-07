
public class ControleRemoto {

	public static void main(String[] args) {

		Televisao tv = new Televisao(0, 0);

		tv.aumentarCanal();
		tv.aumentarVolume();
		
		tv.escolherCanal(5);
		
		System.out.println(tv.consultarCanalVolume());
	}

}
