
public class Televisao {
	private int volume;
	private int canal;
	
	public Televisao(int volume, int canal) {
		setVolume(volume);
		setCanal(canal);
	}
	public int getVolume() {
		return volume;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}
	public int getCanal() {
		return canal;
	}
	public void setCanal(int canal) {
		this.canal = canal;
	}
	
	public int aumentarVolume() {
		volume++;
		return volume;
	}
	public int reduzirVolume() {
		volume--;
		return volume;
	}
	
	public int aumentarCanal() {
		canal++;
		return canal;
	}
	public int reduzirCanal() {
		canal--;
		return canal;
	}
	
	public int escolherCanal(int novoCanal) {
		canal = novoCanal;
		return canal;
	}
	
	public String consultarCanalVolume() {
		String resposta = "Canal: " + canal + "\n" + "Volume: " + volume;
		return resposta;
	}
	
}
